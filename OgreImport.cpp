//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/
#include "OgreTools.h"
#include "OgreImport.h"
#include "XkTextCodec.h"
#include <XkDebugStr.h>
#include <modstack.h>
#include <iskin.h>

#define OgreImport_CLASS_ID	Class_ID(0xbf073213, 0x764ad26a)


class OgreImport : public SceneImport {
	public:

		static HWND hParams;
		
		int				ExtCount();					// Number of extensions supported
		const TCHAR *	Ext(int n);					// Extension #n (i.e. "3DS")
		const TCHAR *	LongDesc();					// Long ASCII description (i.e. "Autodesk 3D Studio File")
		const TCHAR *	ShortDesc();				// Short ASCII description (i.e. "3D Studio")
		const TCHAR *	AuthorName();				// ASCII Author name
		const TCHAR *	CopyrightMessage();			// ASCII Copyright message
		const TCHAR *	OtherMessage1();			// Other message #1
		const TCHAR *	OtherMessage2();			// Other message #2
		unsigned int	Version();					// Version number * 100 (i.e. v3.01 = 301)
		void			ShowAbout(HWND hWnd);		// Show DLL's "About..." box
		int				DoImport(const TCHAR *name,ImpInterface *i,Interface *gi, BOOL suppressPrompts=FALSE);	// Import file
		
		//Constructor/Destructor
		OgreImport();
		~OgreImport();		

};



class OgreImportClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL /*loading = FALSE*/) 		{ return new OgreImport(); }
	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() 				{ return SCENE_IMPORT_CLASS_ID; }
	virtual Class_ID ClassID() 						{ return OgreImport_CLASS_ID; }
	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() 			{ return _T("OgreImport"); }	// returns fixed parsable name (scripter-visible name)
	virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
	

};


ClassDesc2* GetOgreImportDesc() { 
	static OgreImportClassDesc OgreImportDesc;
	return &OgreImportDesc; 
}




INT_PTR CALLBACK OgreImportOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam) {
	static OgreImport *imp = NULL;

	switch(message) {
		case WM_INITDIALOG:
			imp = (OgreImport *)lParam;
			CenterWindow(hWnd,GetParent(hWnd));
			return TRUE;

		case WM_CLOSE:
			EndDialog(hWnd, 0);
			return 1;
	}
	return 0;
}


//--- OgreImport -------------------------------------------------------
OgreImport::OgreImport()
{
	
}

OgreImport::~OgreImport() 
{
	
}

int OgreImport::ExtCount()
{
	//#pragma message(TODO("Returns the number of file name extensions supported by the plug-in."))
	return 2;
}

const TCHAR *OgreImport::Ext(int n)
{		
	//#pragma message(TODO("Return the 'i-th' file name extension (i.e. \"3DS\")."))
	switch(n)
	{
	case 0:
		return _T("mesh");
	}
	return _T("skeleton");
}

const TCHAR *OgreImport::LongDesc()
{
	//#pragma message(TODO("Return long ASCII description (i.e. \"Targa 2.0 Image File\")"))
	return _T("Ogre3D Mesh, Skeleton File");
}
	
const TCHAR *OgreImport::ShortDesc() 
{			
	//#pragma message(TODO("Return short ASCII description (i.e. \"Targa\")"))
	return _T("Ogre3D");
}

const TCHAR *OgreImport::AuthorName()
{			
	//#pragma message(TODO("Return ASCII Author name"))
	return _T("cloud");
}

const TCHAR *OgreImport::CopyrightMessage() 
{	
	//#pragma message(TODO("Return ASCII Copyright message"))
	return _T("");
}

const TCHAR *OgreImport::OtherMessage1() 
{		
	//TODO: Return Other message #1 if any
	return _T("");
}

const TCHAR *OgreImport::OtherMessage2() 
{		
	//TODO: Return other message #2 in any
	return _T("");
}

unsigned int OgreImport::Version()
{				
	//#pragma message(TODO("Return Version number * 100 (i.e. v3.01 = 301)"))
	return 100;
}

void OgreImport::ShowAbout(HWND hWnd)
{			
	// Optional
}

class FindSkinOnStack : public GeomPipelineEnumProc
	{
public:  
   FindSkinOnStack() : mFound(false), mMod(NULL),mDerObj(NULL),mIndex(-1) {}
   PipeEnumResult proc(ReferenceTarget *object, IDerivedObject *derObj, int index);
   bool mFound;
   //what we basically return, the derived object and the index.
   IDerivedObject *mDerObj;
   int mIndex;
   Modifier *mMod;
protected:
   FindSkinOnStack(FindSkinOnStack& rhs); // disallowed
   FindSkinOnStack & operator=(const FindSkinOnStack& rhs); // disallowed
};

PipeEnumResult FindSkinOnStack::proc(
   ReferenceTarget *object, 
   IDerivedObject *derObj, 
   int index)
{
   MSTR str;
	object->GetClassName(str);
	const wchar_t* wszName = str.data();
	char szName[256] = { 0 };
	Xk::TextCodec::unicodeToAscii(wszName, szName, 256);
	DBGSTRING("CurName: %s", szName);
	if(derObj == NULL)
		DBGSTRING("DerivedObject is NULL");
	else
		DBGSTRING("DerivedObject valid");

   ModContext* curModContext = NULL;
   if ((derObj != NULL) && (object && object->ClassID() == SKIN_CLASSID))  
   {
	   mDerObj = derObj;
	   mIndex = index;
	   mMod = dynamic_cast<Modifier*>(object);
	   mFound = true;
       return PIPE_ENUM_STOP;
	}
   return PIPE_ENUM_CONTINUE;
}

int OgreImport::DoImport(const TCHAR *filename,ImpInterface *i,
						Interface *gi, BOOL suppressPrompts)
{
	#pragma message(TODO("Implement the actual file import here and"))

	char szFileName[MAX_PATH] = { 0 };
	Xk::TextCodec::unicodeToAscii(filename, szFileName, MAX_PATH);

	char szDrive[MAX_PATH] = { 0 };
	char szDir[MAX_PATH] = { 0 };
	char szFile[MAX_PATH] = { 0 };
	char szExt[32] = { 0 };

	_splitpath(szFileName, szDrive, szDir, szFile, szExt);
	std::string strExt = szExt;
	if(strExt == ".mesh") {
		DBGSTRING("����Ogre Mesh");
		OgreTools::importMesh(szFileName, i, gi);
	}else if(strExt == ".skeleton") {
		DBGSTRING("����Ogre Skeleton");
		OgreTools::importSkeleton(szFileName, i, gi);
	}

	if(!suppressPrompts)
		DialogBoxParam(hInstance, 
				MAKEINTRESOURCE(IDD_PANEL), 
				GetActiveWindow(), 
				OgreImportOptionsDlgProc, (LPARAM)this);

	//#pragma message(TODO("return TRUE If the file is imported properly"))
	return TRUE;
}
	
