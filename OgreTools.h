#pragma once

#include <Ogre.h>
#include "XkSkeletonSerializerEx.h"
#include "XkSkeletonManagerEx.h"
#include <OgreDefaultHardwareBufferManager.h>

#include <max.h>

class OgreTools
{
private:
	OgreTools() {}

public:
	static bool create();
	static void destroy();

	static bool importMesh(const char* szFileName, ImpInterface* i, Interface* gi);
	static bool importSkeleton(const char* szFileName, ImpInterface* i, Interface* gi);
	static bool writeBone(Ogre::Bone* pBone, ImpInterface* i, Interface* gi);
	
	static Point3 convertPoint3(const Ogre::Vector3& vecPos);
	static Quat convertQuat(const Ogre::Quaternion& q);


private:
	static Ogre::LogManager* logMgr;
	static Ogre::Math* mth;
	static Ogre::LodStrategyManager* lodMgr;
	static Ogre::MaterialManager* matMgr;
	static Xk::SkeletonManagerEx* skelMgr;
	static Ogre::MeshSerializer* meshSerializer;
	static Xk::SkeletonSerializerEx* skeletonSerializer;
	static Ogre::DefaultHardwareBufferManager* bufferMgr;
	static Ogre::MeshManager* meshMgr;
	static Ogre::ResourceGroupManager* resGroupMgr;
};