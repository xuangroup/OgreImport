#include "OgreTools.h"
#include "XkDebugStr.h"
#include "XkTextCodec.h"

Ogre::LogManager* OgreTools::logMgr = 0;
Ogre::Math* OgreTools::mth = 0;
Ogre::LodStrategyManager* OgreTools::lodMgr = 0;
Ogre::MaterialManager* OgreTools::matMgr = 0;
Xk::SkeletonManagerEx* OgreTools::skelMgr = 0;
Ogre::MeshSerializer* OgreTools::meshSerializer = 0;
Xk::SkeletonSerializerEx* OgreTools::skeletonSerializer = 0;
Ogre::DefaultHardwareBufferManager* OgreTools::bufferMgr = 0;
Ogre::MeshManager* OgreTools::meshMgr = 0;
Ogre::ResourceGroupManager* OgreTools::resGroupMgr = 0;

bool OgreTools::create()
{
	DBG_BEGIN_END0();
	try
	{
		logMgr = new Ogre::LogManager();
		logMgr->createLog("Temporary log", false, true, true);
		logMgr->setDefaultLog(logMgr->createLog("OgreImport", false));
		logMgr->destroyLog("Temporary log");

		resGroupMgr = new Ogre::ResourceGroupManager();
		mth = new Ogre::Math();
		lodMgr = new Ogre::LodStrategyManager();
		meshMgr = new Ogre::MeshManager();
		matMgr = new Ogre::MaterialManager();
		matMgr->initialise();
		skelMgr = new Xk::SkeletonManagerEx();
		meshSerializer = new Ogre::MeshSerializer();
		skeletonSerializer = new Xk::SkeletonSerializerEx();
		bufferMgr = new Ogre::DefaultHardwareBufferManager();

	}
	catch(Ogre::Exception& e)
	{
		XDBGSTRING("create failed!");
		return false;
	}

	return true;
}

void OgreTools::destroy()
{
	DBG_BEGIN_END0();

	delete skeletonSerializer;
	delete meshSerializer;
	delete skelMgr;
	delete matMgr;
	delete meshMgr;
	delete lodMgr;
	delete mth;
	delete resGroupMgr;
	delete bufferMgr;
	delete logMgr;
}

bool OgreTools::importMesh(const char* szFileName, ImpInterface* i, Interface* gi)
{
	DBG_BEGIN_END0();

	char szDrive[MAX_PATH] = { 0 };
	char szDir[MAX_PATH] = { 0 };
	char szName[MAX_PATH] = { 0 };
	char szExt[MAX_PATH] = { 0 };

	_splitpath(szFileName, szDrive, szDir, szName, szExt);

	std::ifstream ifs;
	ifs.open(szFileName, std::ios_base::in | std::ios_base::binary);
	if(ifs.bad()) {
		DBGSTRING("Unable to load file: %s", szFileName);
		return false;
	}

	Ogre::DataStreamPtr stream(new Ogre::FileStreamDataStream(szFileName, &ifs, false));
	Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().create(szName,
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	meshSerializer->importMesh(stream, mesh.getPointer());

	int nVertexCount = 0;
	int nIndexCount = 0;
	for(int nIndex=0; nIndex<mesh->getNumSubMeshes(); nIndex++) {
        Ogre::SubMesh* pSubMesh = mesh->getSubMesh(nIndex);
        if(pSubMesh->useSharedVertices) {
            DBGSTRING("useSharedVertices");
            nVertexCount += mesh->sharedVertexData->vertexCount;
        }else{
            nVertexCount += pSubMesh->vertexData->vertexCount;
        }
        nIndexCount += pSubMesh->indexData->indexCount;
        DBGSTRING("nIndexCount: %d", pSubMesh->indexData->indexCount);
    }

	TriObject* pTri = CreateNewTriObject();
	Mesh* maxMesh = CreateNewMesh();
	maxMesh->setNumVerts(nVertexCount);
	maxMesh->setNumFaces(nIndexCount / 3);
	maxMesh->setNumTVerts(nVertexCount);
	maxMesh->setNumTVFaces(nIndexCount / 3);

	for(int nIndex=0; nIndex<mesh->getNumSubMeshes(); nIndex++) 
	{
		DBGSTRING("SubMesh: %d", nIndex);
		Ogre::SubMesh* pSubMesh = mesh->getSubMesh(nIndex);
		Ogre::VertexData* pVertexData = pSubMesh->useSharedVertices ? mesh->sharedVertexData : pSubMesh->vertexData;

		const Ogre::VertexElement* posElem = pVertexData->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);
        const Ogre::VertexElement* uvElem = pVertexData->vertexDeclaration->findElementBySemantic(Ogre::VES_TEXTURE_COORDINATES);

		Ogre::HardwareVertexBufferSharedPtr vbuf = pVertexData->vertexBufferBinding->getBuffer(posElem->getSource());
        Ogre::HardwareVertexBufferSharedPtr uvbuf = pVertexData->vertexBufferBinding->getBuffer(uvElem->getSource());
        
        unsigned char* pVertex = static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        
        float* pReal;
        float* pUVReal;
        //读取顶点
        for(size_t j=0; j<pVertexData->vertexCount; ++j, pVertex += vbuf->getVertexSize()) {
            posElem->baseVertexPointerToElement(pVertex, &pReal);
            //Ogre::Vector3 pos(pReal[0], pReal[1], pReal[2]);
			maxMesh->setVert(j, pReal[0], -pReal[2], pReal[1]);
        }
        vbuf->unlock();

		//读取索引
		std::vector<unsigned long> vecIndex;
		Ogre::IndexData* pIndexData = pSubMesh->indexData;
        Ogre::HardwareIndexBufferSharedPtr ibuf = pIndexData->indexBuffer;
        bool b32Bit = ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT;

        unsigned char* pIndex = static_cast<unsigned char*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        int nIndexSize = b32Bit ? 4 : 2;

        for(size_t k=0; k<pIndexData->indexCount; k++) {
            unsigned long ulIndex = 0;
            if(b32Bit) {
                memcpy(&ulIndex, pIndex, 4);
            }else{
                unsigned short usIndex = 0;
                memcpy(&usIndex, pIndex, 2);
                ulIndex = static_cast<unsigned long>(usIndex);
            }

            vecIndex.push_back(ulIndex);
            pIndex += nIndexSize;
        }
        ibuf->unlock();

		//读取UV
        unsigned char* pUV = static_cast<unsigned char*>(uvbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        for(size_t j=0; j<pVertexData->vertexCount; ++j, pUV += uvbuf->getVertexSize()) {
            uvElem->baseVertexPointerToElement(pUV, &pUVReal);
			maxMesh->setTVert(j, pUVReal[0], 1.0f - pUVReal[1], 0);
        }
        uvbuf->unlock();

		for(int nIndex = 0; nIndex < maxMesh->getNumFaces(); nIndex++) {
			int nRet = nIndex*3;
			if(nRet >= vecIndex.size()) break;

			Face* fp = &maxMesh->faces[nIndex];
			fp->setVerts(vecIndex[nRet], vecIndex[nRet+1], vecIndex[nRet+2]);
			TVFace* tvfp = &maxMesh->tvFace[nIndex];
			tvfp->setTVerts(vecIndex[nRet], vecIndex[nRet+1], vecIndex[nRet+2]);
		}
	}

	ImpNode* pNode = i->CreateNode();
	pTri->mesh = *maxMesh;
	pNode->Reference(pTri);

	wchar_t wszName[256] = { 0 };
	Ogre::String strMeshName = mesh->getName();
	Xk::TextCodec::asciiToUnicode(strMeshName.c_str(), wszName, 256);
	pNode->SetName(wszName);
	
	i->AddNodeToScene(pNode);

	Ogre::MeshManager::getSingleton().remove(szName);

	return true;
}

bool OgreTools::importSkeleton(const char* szFileName, ImpInterface* i, Interface* gi)
{
	std::ifstream ifs;
	ifs.open(szFileName, std::ios_base::in | std::ios_base::binary);
	if(ifs.bad()) {
		DBGSTRING("Unable to load file: %s", szFileName);
		return false;
	}

	Ogre::SkeletonPtr skel = Ogre::SkeletonManager::getSingleton().create("conversion",
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	Ogre::DataStreamPtr stream(new Ogre::FileStreamDataStream(szFileName, &ifs, false));
	skeletonSerializer->importSkeleton(stream, skel.getPointer());

	DBGSTRING("Bone Num: %d", skel->getNumBones());
	for(int nIndex=0; nIndex<skel->getNumBones(); nIndex++) {
		DBGSTRING("BoneName: %s", skel->getBone(nIndex)->getName().c_str());
		writeBone(skel->getBone(nIndex), i, gi);

	}

	return true;
}

bool OgreTools::writeBone(Ogre::Bone* pBone, ImpInterface* i, Interface* gi)
{
	//创建Bone
	Object* pMaxBone = (Object*)gi->CreateInstance(GEOMOBJECT_CLASS_ID, BONE_OBJ_CLASSID);
	
	INode* pNode = gi->CreateObjectNode(pMaxBone);
	wchar_t wszName[256] = { 0 };
	Xk::TextCodec::asciiToUnicode(pBone->getName().c_str(), wszName, 256);
	pNode->SetName(wszName);

	Point3 pos = convertPoint3(pBone->getPosition());
	Quat quat = convertQuat(pBone->getOrientation());

	Matrix3 matBase;
	matBase.IdentityMatrix();
	matBase.SetTrans(pos);
	
	pNode->SetNodeTM(0, matBase);
	
	return true;
}

Point3 OgreTools::convertPoint3(const Ogre::Vector3& vecPos)
{
	Point3 p(vecPos.x, -vecPos.z, vecPos.y);
	return p;
}

Quat OgreTools::convertQuat(const Ogre::Quaternion& q)
{
	Quat quat(q.x, q.y, q.z, q.w);
	return quat;
}